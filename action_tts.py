import base64
import hashlib
import os

import synthesize
from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

input_topic = "Jaco/SayText"
output_topic_base = "Jaco/{}/AudioWav"

speaker_id = 38
cache_path = "/cache/"
audio_speed = 1.0
audio_var_a = 0.5
audio_var_b = 0.7

# ==================================================================================================


def on_connect(client):
    client.subscribe(input_topic)


# ==================================================================================================


def on_message(client, userdata, msg):
    print("Received message on topic:", msg.topic)

    if msg.topic == input_topic:
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
        text = payload["data"]
        output_topic = output_topic_base.format(payload["satellite"].capitalize())

        print("Sending to {} the audio for the text: {}".format(output_topic, text))

        wav_content = create_and_read(text)
        wav_content = base64.b64encode(wav_content).decode()
        payload = {
            "data": wav_content,
            "timestamp": payload["timestamp"],
            "type": "speech",
        }
        msg_out = comm_tools.encrypt_msg(payload, output_topic)
        client.publish(output_topic, msg_out)

        print("Sent the audio to {}".format(output_topic))


# ==================================================================================================


def create_and_read(text):
    cache_id = "{}_{}".format(text, speaker_id)
    cache_id = hashlib.sha256(cache_id.encode("utf-8")).hexdigest()
    cache_file = cache_path + cache_id + ".wav"

    if not os.path.exists(cache_file):
        synthesize.synthesize(
            text=text,
            speakerid=speaker_id,
            savepath=cache_file,
            speed=audio_speed,
            va=audio_var_a,
            vb=audio_var_b,
        )
    else:
        print("Using file from cache")

    with open(cache_file, "rb") as file:
        content = file.read()

    return content


# ==================================================================================================


def main():
    config = utils.load_global_config()
    client = comm_tools.connect_mqtt_client(config, on_connect, on_message)
    client.loop_forever()


# ==================================================================================================

if __name__ == "__main__":
    main()
