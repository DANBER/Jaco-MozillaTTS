import argparse
import os
import sys
import time

sys.path.append("/GameTTS/GameTTS/")
import main as gametts
from app.utils import save_audio

# ==================================================================================================


def synthesize(text, speakerid, savepath, speed, va, vb):

    params = {"speech_speed": speed, "speech_var_a": va, "speech_var_b": vb}
    starttime = time.time()

    audio_data = gametts.synthesizer.synthesize(text, speakerid, params)
    save_audio(
        os.path.dirname(savepath),
        os.path.basename(savepath).split(".")[0],
        audio_data,
        "wav",
    )

    duration = time.time() - starttime
    print("Generating the audio took {:.3f} seconds".format(duration))


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Synthesize some text")
    parser.add_argument("--text", type=str, help="Text")
    parser.add_argument("--speakerid", type=int, help="Speaker ID")
    parser.add_argument("--savepath", type=str, help="Save to path")
    parser.add_argument("--speed", type=float, help="Speaking speed")
    parser.add_argument("--va", type=float, help="Variance A")
    parser.add_argument("--vb", type=float, help="Variance B")
    args = parser.parse_args()

    if not any(list(vars(args).values())):
        print("Run with '--help' or '-h' for argument explanations")

    synthesize(args.text, args.speakerid, args.savepath, args.speed, args.va, args.vb)


# ==================================================================================================

if __name__ == "__main__":
    main()
